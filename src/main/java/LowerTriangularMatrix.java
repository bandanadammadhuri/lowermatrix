import java.util.Scanner;

public class LowerTriangularMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        int rowsOfMatrix = scanner.nextInt();
        int columnsOfMatrix = scanner.nextInt();
        int[][] arrayMatrix = new int[rowsOfMatrix][columnsOfMatrix];
        for (int i = 0; i < rowsOfMatrix; i++) {
            for (int j = 0; j < columnsOfMatrix; j++) {
                arrayMatrix[i][j] = scanner.nextInt();
                if (j > i && arrayMatrix[i][j] != 0) {
                    count = 1;
                }
            }
        }
        if (count == 0)
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
